from setuptools import setup
from setuptools_rust import Binding, RustExtension

setup(
    name="lisa",
    version="0.1.0",
    rust_extensions=[RustExtension("lisa", binding=Binding.PyO3)],
   #     packages=["src"],
    # rust extensions are not zip safe, just like C-extensions.
    zip_safe=False,
)
