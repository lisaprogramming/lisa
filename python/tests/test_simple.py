import pytest
import lisa

def test_simple_program():
    instructions = lisa.Instruction.parse("create var a\na=5")
    interpreter = lisa.Interpreter(None)
    state = interpreter.run(instructions)
    assert state.get('a') == 5

def test_syntaxerror_program():
    with pytest.raises(lisa.SyntaxError):
       instructions = lisa.Instruction.parse("create var")

def test_simple_program():
    instructions = lisa.Instruction.parse("create var a\nb=5")
    interpreter = lisa.Interpreter(None)
    with pytest.raises(lisa.RuntimeError):
        state = interpreter.run(instructions)


def test_simple_library():
    class Namespace(dict):
        def clone(self):
            return Namespace(self.copy())

    class SimpleLibrary:
        def create_context(self):
            return Namespace({'a': 0})

        def inc(self, context):
            context['a'] += 1

    instructions = lisa.Instruction.parse("inc()\ninc()")
    interpreter = lisa.Interpreter(SimpleLibrary())
    state = interpreter.run(instructions)
    assert state.get_context()['a'] == 2


def test_noclone_innamespace():
    class SimpleLibrary:
        def create_context(self):
            return {'a': 0}

        def inc(self, context):
            context['a'] += 1

    instructions = lisa.Instruction.parse("inc()\ninc()")
    interpreter = lisa.Interpreter(SimpleLibrary())
    with pytest.raises(Exception):
        state = interpreter.run(instructions)



