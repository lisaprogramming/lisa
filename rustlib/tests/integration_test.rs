use lisa;
use std::rc::Rc;

#[test]
fn invalid_indent0() {
    let t = "create var a\n   a = 5";
    let out = lisa::parser::parse_source(&t);
    println!("{:?}", out);
    out.expect_err("Parsing should fail");
}

#[test]
fn invalid_indent1() {
    let t = "if 5 < 6\na = 5";
    let out = lisa::parser::parse_source(&t);
    println!("{:?}", out);
    out.expect_err("Parsing should fail");
}

#[test]
fn missing_child() {
    let t = "if 5 < 6";
    let out = lisa::parser::parse_source(&t);
    println!("{:?}", out);
    out.expect_err("Parsing should fail");
}

#[test]
fn run_empty_program0() {
    let t = "";
    lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
}

#[test]
fn run_simple_program0() {
    let t = "create var a
# A comment

a = 5";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    assert_eq!(i.steps.len(), 2);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
}

#[test]
fn run_simple_program2() {
    let t = "create var a\na = 5\na = 6";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    assert_eq!(i.steps.len(), 3);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // a = 6
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
}

#[test]
fn run_simple_program3() {
    let t = "create var a\ncreate var b\na = 5\na = a+6\nb = 2\nb = a * b";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(11)
    );
    assert_eq!(
        state.namespace.get("b").unwrap(),
        lisa::interpreter::VariableValue::Int(22)
    );
    assert_eq!(i.steps.len(), 6);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // create var b
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 2);
    assert!(st.namespace.get("a").is_err());
    assert!(st.namespace.get("b").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    assert!(st.namespace.get("b").is_err());
    // a = a+6
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(11)
    );
    assert!(st.namespace.get("b").is_err());
    // b = 2
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 4);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(11)
    );
    assert_eq!(
        st.namespace.get("b").unwrap(),
        lisa::interpreter::VariableValue::Int(2)
    );
    // b = a * b
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 5);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(11)
    );
    assert_eq!(
        st.namespace.get("b").unwrap(),
        lisa::interpreter::VariableValue::Int(22)
    );
}

#[test]
fn run_if_program() {
    let t = "create var a
a = 5
if a == 5
    a = 6";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    assert_eq!(i.steps.len(), 4);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // if a == 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    //  a = 6
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
}

#[test]
fn run_if_program2() {
    let t = "create var a
a = 5
if a != 5
    a = 6";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    assert_eq!(i.steps.len(), 3);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // if a != 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
}

#[test]
fn run_if_program3() {
    let t = "create var a
a = 5
if a == 5
    a = 6
    a = a/2";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(3)
    );
    assert_eq!(i.steps.len(), 5);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // if a == 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // a = 6
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    // a = a/2
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 4);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(3)
    );
}

#[test]
fn run_if_program4() {
    let t = "create var a
create var b
a = 5
if a == 5
    a = 6
if a == 6
    b = 2 ";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    assert_eq!(
        state.namespace.get("b").unwrap(),
        lisa::interpreter::VariableValue::Int(2)
    );
    assert_eq!(i.steps.len(), 7);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // create var b
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 2);
    assert!(st.namespace.get("a").is_err());
    assert!(st.namespace.get("b").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    assert!(st.namespace.get("b").is_err());
    // if a == 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    assert!(st.namespace.get("b").is_err());
    // a = 6
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 4);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    assert!(st.namespace.get("b").is_err());
    // if a == 6
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 5);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    assert!(st.namespace.get("b").is_err());
    // b = 2
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 6);
    assert_eq!(st.namespace.len(), 2);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    assert_eq!(
        st.namespace.get("b").unwrap(),
        lisa::interpreter::VariableValue::Int(2)
    );
}

#[test]
fn run_loop_program0() {
    let t = "create var a
a = 5
loop 0 times
    a = 6
a = 4";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(4)
    );
    assert_eq!(i.steps.len(), 4);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // loop 0 times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // a = 4
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 4);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(4)
    );
}

#[test]
fn run_loop_program1() {
    let t = "create var a
a = 5
loop 1 times
    a = 6
a = a+1";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(7)
    );
    assert_eq!(i.steps.len(), 6);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // loop 1 times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // a = 6
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    // loop 1 times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    // a = a + 1
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 4);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(7)
    );
}

#[test]
fn run_loop_program_a() {
    let t = "create var a
a = 5
loop a times
    a = a + 1
a = a+1";
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(None);
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        state.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(11)
    );
    assert_eq!(i.steps.len(), 14);
    let mut iter = i.steps.iter();
    // create var a
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 1);
    assert!(st.namespace.get("a").is_err());
    // a = 5
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 1);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // loop a times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(5)
    );
    // a = a + 1
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    // loop a times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(6)
    );
    // a = a + 1
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(7)
    );
    // loop a times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(7)
    );
    // a = a + 1
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(8)
    );
    // loop a times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(8)
    );
    // a = a + 1
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(9)
    );
    // loop a times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(9)
    );
    // a = a + 1
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 3);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(10)
    );
    // loop a times
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 2);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(10)
    );
    // a = a + 1
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 4);
    assert_eq!(st.namespace.len(), 1);
    assert_eq!(
        st.namespace.get("a").unwrap(),
        lisa::interpreter::VariableValue::Int(11)
    );
}

#[test]
fn run_builtin_call_program() {
    let t = "print(5, 6, 5+8)";
    let l = Rc::new(lisa::libraries::printlib::PrintLibrary::new());
    let p = lisa::parser::parse_source(&t).unwrap_or_else(|e| panic!("{}", e));
    let mut i = lisa::interpreter::Interpreter::new(Some(l));
    let state = i.run(p.as_ref().unwrap()).unwrap();
    assert_eq!(
        lisa::libraries::printlib::PrintLibrary::get_context_from_state(&state).unwrap(),
        "5 6 13 "
    );
    assert_eq!(i.steps.len(), 1);
    let mut iter = i.steps.iter();
    // print(a, 6, 5+8)
    let (cl, st) = iter.next().unwrap();
    assert_eq!(*cl, 0);
    assert_eq!(st.namespace.len(), 0);
}
