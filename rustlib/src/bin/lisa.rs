use std::error::Error;
use std::fs;
use std::rc::Rc;
#[macro_use]
extern crate clap;

use lisa::{interpreter::Interpreter, libraries::printlib::PrintLibrary, parser};

fn main() -> Result<(), Box<dyn Error>> {
    let matches = clap_app!(myapp =>
        (version: "0.1")
        (author: "Matthieu Gautier <mgautier@kymeria.fr>")
        (about: "Run a lisa program using the print library.")
        (@arg INPUT: +required "The source file to use")
    )
    .get_matches();

    let filename = matches.value_of("INPUT").unwrap();
    let contents = fs::read_to_string(filename)?;

    let library = Rc::new(PrintLibrary::new());
    let program = parser::parse_source(&contents)?;
    let mut interpreter = Interpreter::new(Some(library));
    let state = interpreter.run(&program.unwrap())?;

    let string_context = PrintLibrary::get_context_from_state(&state).unwrap();
    println!("{}", string_context);
    Ok(())
}
