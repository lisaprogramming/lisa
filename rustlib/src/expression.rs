use crate::interpreter::{Namespace, VariableError, VariableValue};
use std::fmt;
use std::rc::Rc;

pub trait Expression: fmt::Debug {
    fn get_value(&self, namespace: Rc<Namespace>) -> Result<VariableValue, VariableError>;
}
pub type DynExpression = Box<dyn Expression>;

#[derive(Debug)]
pub struct Literal {
    text: String,
}
impl Literal {
    pub fn new(text: &str) -> Self {
        Literal {
            text: text.to_string(),
        }
    }
}
impl Expression for Literal {
    fn get_value(&self, _: Rc<Namespace>) -> Result<VariableValue, VariableError> {
        match self.text.parse::<u32>() {
            Ok(i) => Ok(VariableValue::Int(i)),
            Err(_) => panic!(),
        }
    }
}

#[derive(Debug)]
pub struct Identifier {
    text: String,
}
impl Identifier {
    pub fn new(text: &str) -> Self {
        Identifier {
            text: text.to_string(),
        }
    }
}
impl Expression for Identifier {
    fn get_value(&self, namespace: Rc<Namespace>) -> Result<VariableValue, VariableError> {
        namespace.get(&self.text)
    }
}

#[derive(Debug)]
pub enum OperatorKind {
    ADD,
    SUB,
    DIV,
    MUL,
}

#[derive(Debug)]
pub struct Operator {
    lh: DynExpression,
    rh: DynExpression,
    op: OperatorKind,
}
impl Operator {
    pub fn new(op: OperatorKind, lh: DynExpression, rh: DynExpression) -> Self {
        Operator { op, lh, rh }
    }
}
impl Expression for Operator {
    fn get_value(&self, namespace: Rc<Namespace>) -> Result<VariableValue, VariableError> {
        let rh = match self.rh.get_value(Rc::clone(&namespace))? {
            VariableValue::Int(i) => i,
            _ => panic!(),
        };
        let lh = match self.lh.get_value(Rc::clone(&namespace))? {
            VariableValue::Int(i) => i,
            _ => panic!(),
        };
        Ok(VariableValue::Int(match &self.op {
            OperatorKind::ADD => lh + rh,
            OperatorKind::SUB => lh - rh,
            OperatorKind::DIV => lh / rh,
            OperatorKind::MUL => lh * rh,
        }))
    }
}

#[derive(Debug)]
pub enum BOperatorKind {
    EQ,
    NEQ,
    LT,
    GT,
    LTE,
    GTE,
}

#[derive(Debug)]
pub struct BOperator {
    rh: DynExpression,
    lh: DynExpression,
    op: BOperatorKind,
}
impl BOperator {
    pub fn new(op: BOperatorKind, lh: DynExpression, rh: DynExpression) -> Self {
        BOperator { op, lh, rh }
    }
}
impl Expression for BOperator {
    fn get_value(&self, namespace: Rc<Namespace>) -> Result<VariableValue, VariableError> {
        let rh = match self.rh.get_value(Rc::clone(&namespace))? {
            VariableValue::Int(i) => i,
            _ => panic!(),
        };
        let lh = match self.lh.get_value(Rc::clone(&namespace))? {
            VariableValue::Int(i) => i,
            _ => panic!(),
        };
        Ok(VariableValue::Bool(match &self.op {
            BOperatorKind::EQ => lh == rh,
            BOperatorKind::NEQ => lh != rh,
            BOperatorKind::LT => lh < rh,
            BOperatorKind::GT => lh > rh,
            BOperatorKind::LTE => lh <= rh,
            BOperatorKind::GTE => lh >= rh,
        }))
    }
}
