use super::Library;
use crate::expression::DynExpression;
use crate::interpreter::{State, Steps};
use std::any::Any;
use std::error::Error;
use std::fmt::Write;
use std::rc::Rc;

pub struct PrintLibrary {}
impl PrintLibrary {
    pub fn new() -> Self {
        PrintLibrary {}
    }
    pub fn get_context_from_state(state: &State) -> Option<String> {
        match state.lib_context.downcast_ref::<String>() {
            Some(as_string) => Some(String::from(as_string.as_str())),
            _ => None,
        }
    }
}
impl Library for PrintLibrary {
    fn run_method(
        &self,
        method_name: &str,
        arguments: &[DynExpression],
        mut state: State,
        cl: usize,
        steps: &mut Steps,
    ) -> Result<State, Box<dyn Error>> {
        match method_name {
            "print" => {
                let mut content = match state.lib_context.downcast_ref::<String>() {
                    Some(as_string) => String::from(as_string.as_str()),
                    _ => unreachable!(),
                };
                for a in arguments.iter() {
                    write!(content, "{} ", a.get_value(Rc::clone(&state.namespace))?)?;
                }
                state.lib_context = Rc::new(content);
                steps.push(cl, state.clone());
                Ok(state)
            }
            _ => unreachable!(),
        }
    }
    fn create_context(&self) -> Rc<dyn Any> {
        Rc::new(String::new())
    }
    fn duplicate_context(&self, context: &dyn Any) -> Rc<dyn Any> {
        match context.downcast_ref::<String>() {
            Some(as_string) => Rc::new(String::from(as_string.as_str())),
            _ => unreachable!(),
        }
    }
}
