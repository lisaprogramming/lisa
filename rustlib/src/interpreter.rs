use crate::instruction::{Instruction, InstructionKind};
use crate::libraries::Library;
use std::any::Any;
use std::collections::HashSet;
use std::error::Error;
use std::fmt;
use std::rc::Rc;
use std::slice::Iter;

#[derive(Debug)]
pub enum ErrorKind {
    MissingLib,
    VariableError(VariableError),
    Other(String),
}

#[derive(Debug)]
pub struct RuntimeError {
    pub line: usize,
    pub error: ErrorKind,
}
impl RuntimeError {
    pub fn new_missing_lib(line: usize) -> Box<Self> {
        Box::new(RuntimeError {
            line,
            error: ErrorKind::MissingLib,
        })
    }
    pub fn new_variable_error(line: usize, other: VariableError) -> Box<Self> {
        Box::new(RuntimeError {
            line,
            error: ErrorKind::VariableError(other),
        })
    }
    pub fn new_other(line: usize, var: &str) -> Box<Self> {
        Box::new(RuntimeError {
            line,
            error: ErrorKind::Other(var.to_string()),
        })
    }
    pub fn new_from_other(line: usize, error: RuntimeError) -> Box<Self> {
        Box::new(RuntimeError {
            line,
            error: error.error,
        })
    }
}
impl fmt::Display for RuntimeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.error {
            ErrorKind::MissingLib => write!(
                f,
                "Line {} : Call to a builtin function but no library defined.",
                self.line
            ),
            ErrorKind::VariableError(var) => write!(f, "Line {} : {}.", self.line, var),
            ErrorKind::Other(var) => write!(f, "Line {} : Error {}.", self.line, var),
        }
    }
}
impl Error for RuntimeError {}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum VariableValue {
    Int(u32),
    Bool(bool),
    None,
}
impl fmt::Display for VariableValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            VariableValue::Int(i) => write!(f, "{}", i),
            VariableValue::Bool(b) => write!(f, "{}", b),
            VariableValue::None => Ok(()),
        }
    }
}

#[derive(Debug)]
pub enum VariableError {
    NonExistant(String),
    AlreadyDefined(String),
    Unset(String),
}
impl VariableError {
    pub fn non_existant(var: &str) -> Self {
        VariableError::NonExistant(var.to_string())
    }
    pub fn already_defined(var: &str) -> Self {
        VariableError::AlreadyDefined(var.to_string())
    }
    pub fn unset(var: &str) -> Self {
        VariableError::Unset(var.to_string())
    }
}
impl fmt::Display for VariableError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            VariableError::NonExistant(s) => write!(f, "{}", s),
            VariableError::AlreadyDefined(s) => write!(f, "{}", s),
            VariableError::Unset(s) => write!(f, "{}", s),
        }
    }
}
impl Error for VariableError {}

/// A namespace contains everything a program has at a moment.
/// Parent is the containing namespace.
pub struct Namespace {
    var_name: Option<String>,
    var_value: VariableValue,
    previous: Option<Rc<Namespace>>,
}
impl Namespace {
    fn new() -> Rc<Namespace> {
        Rc::new(Namespace {
            var_name: None,
            var_value: VariableValue::None,
            previous: None,
        })
    }
    pub fn add(self: Rc<Namespace>, name: &str) -> Result<Rc<Namespace>, VariableError> {
        if self.contains(name) {
            Err(VariableError::already_defined(name))
        } else {
            Ok(Rc::new(Namespace {
                var_name: Some(name.to_string()),
                var_value: VariableValue::None,
                previous: Some(Rc::clone(&self)),
            }))
        }
    }
    pub fn set(
        self: Rc<Namespace>,
        name: &str,
        value: VariableValue,
    ) -> Result<Rc<Namespace>, VariableError> {
        if !self.contains(name) {
            Err(VariableError::non_existant(name))
        } else {
            Ok(Rc::new(Namespace {
                var_name: Some(name.to_string()),
                var_value: value,
                previous: Some(Rc::clone(&self)),
            }))
        }
    }
    pub fn contains(&self, name: &str) -> bool {
        match &self.var_name {
            Some(n) if n == name => true,
            _ => match &self.previous {
                Some(p) => p.contains(name),
                None => false,
            },
        }
    }
    pub fn get(&self, name: &str) -> Result<VariableValue, VariableError> {
        match &self.var_name {
            Some(n) if n == name => match self.var_value {
                VariableValue::None => Err(VariableError::unset(name)),
                _ => Ok(self.var_value),
            },
            _ => match &self.previous {
                Some(p) => p.get(name),
                None => Err(VariableError::non_existant(name)),
            },
        }
    }
    pub fn len(&self) -> usize {
        let mut seen = HashSet::new();
        if let Some(name) = &self.var_name {
            seen.insert(name);
        }
        let mut current = &self.previous;
        while let Some(p) = current {
            if let Some(name) = &p.var_name {
                seen.insert(name);
            }
            current = &p.previous;
        }
        seen.len()
    }
}

/// A state is a state of a running program.
///
/// Where we are and what we contain
#[derive(Clone)]
pub struct State {
    pub namespace: Rc<Namespace>,
    pub lib_context: Rc<dyn Any>,
}
impl State {
    pub fn new(library: &Option<Rc<dyn Library>>) -> Self {
        State {
            namespace: Namespace::new(),
            lib_context: match library {
                Some(l) => l.create_context(),
                None => Rc::new(()),
            },
        }
    }
}

#[derive(Default)]
pub struct Steps {
    pub steps: Vec<(usize, State)>,
}
impl Steps {
    pub fn push(&mut self, cl: usize, state: State) {
        self.steps.push((cl, state))
    }
    pub fn len(&self) -> usize {
        self.steps.len()
    }
    pub fn iter(&self) -> Iter<(usize, State)> {
        self.steps.iter()
    }
}

pub struct Interpreter {
    library: Option<Rc<dyn Library>>,
    pub steps: Steps,
}

impl Interpreter {
    pub fn new(library: Option<Rc<dyn Library>>) -> Interpreter {
        Interpreter {
            library,
            steps: Steps::default(),
        }
    }

    pub fn run(&mut self, instruction: &Instruction) -> Result<State, Box<dyn Error>> {
        let state = State::new(&self.library);
        self.run_instruction(instruction, state)
    }

    fn run_instruction(
        &mut self,
        instruction: &Instruction,
        mut state: State,
    ) -> Result<State, Box<dyn Error>> {
        let mut current = instruction;
        loop {
            state = self.run_one_instruction(current, state)?;
            current = match current.next() {
                None => break (Ok(state)),
                Some(i) => &i,
            };
        }
    }

    fn run_one_instruction(
        &mut self,
        instruction: &Instruction,
        mut state: State,
    ) -> Result<State, Box<dyn Error>> {
        let runtime_error = |e| RuntimeError::new_variable_error(instruction.lineno(), e);
        match &instruction.kind() {
            InstructionKind::CreateVariable(name) => {
                state.namespace = state.namespace.add(&name).map_err(runtime_error)?;
                self.steps.push(instruction.lineno(), state.clone());
                Ok(state)
            }
            InstructionKind::SetValue(name, value) => {
                let value = match value.get_value(Rc::clone(&state.namespace)) {
                    Ok(v) => v,
                    Err(e) => {
                        return Err(RuntimeError::new_variable_error(instruction.lineno(), e))
                    }
                };
                state.namespace = state.namespace.set(&name, value).map_err(runtime_error)?;
                self.steps.push(instruction.lineno(), state.clone());
                Ok(state)
            }
            InstructionKind::BuiltinCall(name, arguments) => match self.library.as_ref() {
                None => Err(RuntimeError::new_missing_lib(instruction.lineno())),
                Some(lib) => Ok(lib.run_method(
                    &name,
                    &arguments,
                    state,
                    instruction.lineno(),
                    &mut self.steps,
                )?),
            },
            InstructionKind::Conditional(condition, inner) => {
                let condition = condition
                    .get_value(Rc::clone(&state.namespace))
                    .map_err(runtime_error)?;
                match condition {
                    VariableValue::Bool(b) => {
                        self.steps.push(instruction.lineno(), state.clone());
                        if b {
                            state = self.run_instruction(inner.as_ref().unwrap(), state)?;
                        };
                        Ok(state)
                    }
                    VariableValue::Int(mut number) => {
                        self.steps.push(instruction.lineno(), state.clone());
                        while number > 0 {
                            state = self.run_instruction(inner.as_ref().unwrap(), state)?;
                            self.steps.push(instruction.lineno(), state.clone());
                            number -= 1;
                        }
                        Ok(state)
                    }
                    VariableValue::None => {
                        Err(RuntimeError::new_other(instruction.lineno(), "Oups"))
                    } // We should raise a variable_unset because if we get a none it is
                      // because the variable exist but is not set.
                }
            }
            InstructionKind::Skip => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_namespace() {
        let ns = Namespace::new();
        let ns = ns.add("foo").unwrap();
        assert!(ns.contains("foo"));
        assert!(ns.get("foo").is_err());
        let ns = ns.set("foo", VariableValue::Int(5)).unwrap();
        assert_eq!(ns.get("foo").unwrap(), VariableValue::Int(5));
        let ns2 = Rc::clone(&ns).set("foo", VariableValue::Int(6)).unwrap();
        assert_eq!(ns.get("foo").unwrap(), VariableValue::Int(5));
        assert_eq!(ns2.get("foo").unwrap(), VariableValue::Int(6));
        assert_eq!(ns2.len(), 1);
        let ns3 = Rc::clone(&ns2).add("bar").unwrap();
        assert_eq!(ns2.len(), 1);
        assert_eq!(ns3.len(), 2);
        assert_eq!(ns3.get("foo").unwrap(), VariableValue::Int(6));
        assert!(ns3.get("bar").is_err());
    }
}
