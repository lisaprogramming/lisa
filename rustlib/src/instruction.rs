use crate::expression::DynExpression;
use crate::parser::Line;
use std::ops::{Deref, DerefMut};

#[derive(Debug)]
pub enum InstructionKind {
    Skip,
    CreateVariable(String),
    SetValue(String, DynExpression),
    BuiltinCall(String, Vec<DynExpression>),
    Conditional(DynExpression, Option<Box<Instruction>>),
}

pub type IndentLevel = usize;

#[derive(Debug)]
pub struct Instruction {
    line: Line,
    indent: IndentLevel,
    next: Option<Box<Instruction>>,
    kind: InstructionKind,
}
impl Instruction {
    pub fn new(
        line: Line,
        indent: IndentLevel,
        kind: InstructionKind,
        next: Option<Box<Instruction>>,
    ) -> Self {
        Instruction {
            line,
            next,
            indent,
            kind,
        }
    }
    pub fn kind(&self) -> &InstructionKind {
        &self.kind
    }
    pub fn kind_mut(&mut self) -> &mut InstructionKind {
        &mut self.kind
    }
    pub fn lineno(&self) -> usize {
        self.line.lineno
    }
    pub fn get_indent(&self) -> IndentLevel {
        self.indent
    }
    pub fn set_next_instruction(&mut self, instruction: Box<Instruction>) {
        self.next = Some(instruction);
    }
    pub fn next(&self) -> Option<&Instruction> {
        self.next.as_ref().map(Deref::deref)
    }
    pub fn next_mut(&mut self) -> Option<&mut Instruction> {
        self.next.as_mut().map(DerefMut::deref_mut)
    }
}
